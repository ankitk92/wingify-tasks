import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class WingifyTask1 {

    public static String CHROME_DRIVER_PATH = "chromedriver.exe";

    private static WebDriver chromeDriver = new ChromeDriver();

    public static void main(String[] args) throws InterruptedException {

        Verifiers verify = new Verifiers();
        WaitHandlers waitHandler = new WaitHandlers();
        try {
            System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);

            int assertionCount = 1; //Keeps a count of assertions

            String wingifyUrl = "https://app.vwo.com/#/analyze/heatmap/129/reports?token=eyJhY2NvdW50X2lkIjo2LCJleHBlcmltZW50X2lkIjoxMjksImNyZWF0ZWRfb24iOjE1MDc3ODk0ODcsInR5cGUiOiJjYW1wYWlnbiIsInZlcnNpb24iOjEsImhhc2giOiJiMzlmMTQ4MWE0ZDMyN2Q4MDllNTM1YzVlNWFjOGVlMCJ9";
            chromeDriver.get(wingifyUrl);

            String heat = "//button[contains(text(), \"View heatmap\")]";

            //Waits for page to load and asserts it.
            WebElement heatMap = waitHandler.explicitWait(heat,chromeDriver);

            chromeDriver.manage().window().maximize();

            // This code is to interact with elements hidden under overlay, executes JS.
            JavascriptExecutor executor = (JavascriptExecutor) chromeDriver;
            executor.executeScript("arguments[0].click();", heatMap);

            Set<String> set = chromeDriver.getWindowHandles();
            Iterator<String> it = set.iterator();

            //Switching focus to page opened in next tab
            while (it.hasNext()) chromeDriver.switchTo().window(it.next());

            List<WebElement> framesList = chromeDriver.findElements(By.tagName("iframe"));

            //ASSERTION 1:
            //Asserts load of new page and checks all frames loaded
            if (!(verify.verifyValueInt(5, framesList.size(), assertionCount++))) chromeDriver.quit();

            String opaqueBeforeClick =
                    chromeDriver.findElement(By.xpath("//iframe[contains(@id,\"element-list-iframe\")]")).getCssValue("opacity");

            //ASSERTION 2:
            //Asserts load of element list and checks it's clickable and not blocked.
            if (!(verify.verifyValueString("1", opaqueBeforeClick, assertionCount++))) chromeDriver.quit();

            String iframeOnLoad = "//iframe[@id=\"heatmap-iframe\"]";
            waitHandler.explicitWait(iframeOnLoad,chromeDriver);

            chromeDriver.switchTo().frame("heatmap-iframe");
            WebElement elementList = chromeDriver.findElement(By.xpath("//span[contains(text(),\"Element List\")]"));
            elementList.click();

            chromeDriver.switchTo().defaultContent();
            chromeDriver.switchTo().frame("element-list-iframe");

            //Clicking on the pricing link in Element List
            chromeDriver.findElement(By.xpath("//td[contains(text(),\"Pricing\")]")).click();
            chromeDriver.switchTo().defaultContent();

            String opaqueAfterClick =
                    chromeDriver.findElement(By.xpath("//iframe[contains(@id,\"element-list-iframe\")]")).getCssValue("opacity");

            //ASSERTION 3:
            //Asserts element list is blocked and checks it's opacity, meaning it's not highlighted anymore.
            if (!(verify.verifyValueString("0.3", opaqueAfterClick, assertionCount++))) chromeDriver.quit();

            if (assertionCount == 3) System.out.println("CASE PASSED");
            else System.out.println("CASE FAILED");
        } catch (Exception e)
        {
            System.out.println("Case failed: "+e);
        }
        chromeDriver.quit();

    }

}





