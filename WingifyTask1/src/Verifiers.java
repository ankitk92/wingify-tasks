public class Verifiers {

    public  boolean verifyValueString(String expected, String actual, int i) {
        if (expected.equals(actual)) {
            System.out.println(expected + " is equal to " + actual + " assertion " + i + " passed");
            return true;
        } else {
            System.out.println(expected + " is equal to " + actual + " assertion " + i + " failed");
            return false;
        }
    }

    public  boolean verifyValueInt(int expected, int actual, int i) {
        if (expected == actual) {
            System.out.println(expected + " is equal to " + actual + " assertion " + i + " passed");
            return true;
        } else {
            System.out.println(expected + " is equal to " + actual + " assertion " + i + " failed");
            return false;
        }
    }
}
