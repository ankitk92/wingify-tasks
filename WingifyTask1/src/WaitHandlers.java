import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHandlers {

    public WebElement explicitWait(String locator, WebDriver chromeDriver) {

        WebDriverWait wait = new WebDriverWait(chromeDriver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));

    }
}
