A group of 5 testers plan to test a mobile application made using phonegap but they are
limited to only 2 devices namely an iPhone 5 and Nokia XL. Are these resources
sufficient?
How should they distribute tasks and achieve better test coverage?

The resources provided to test the application are insufficeint, before looking for ways to do testing and cover maximum platforms. We should check the platforms we intend to support and the most widely used platforms in the market. 
Once we are clear with the requirement of our end users, we can look to cover most of the testing scope covered by implementing one of or a combination of followig options.

1. Use of emulators.
There are a number of emulators for all the OS and their versions. 
Here is chrom extension for example - https://chrome.google.com/webstore/detail/ripple-emulator-beta/geelfhphabnejjhdalkjhgipohgpdnoc

2. Since Android is the major market share holder and we only have iOS and Windows phone with us,we should be focused more on covering it.
To do that we can:
2.1. Work with correct emulators.
2.2. Get refurbished phones more market at cheap prices. 
2.3. Use our personal phones for testing if possible. 

3. Look for external mobile labs or cloud based mobile labs.
Example - https://mobilelabsinc.com/

4. Use of automation.
As we have a team of 5 members with only 2 handheld devices, the team can be broken down into 2 functions. 
Automation Team & Manual Team
The Automation team can focus of developing automation of the app using emulators and one hand held device at a time. 

